module.exports = {
    css: {
        loaderOptions: {
            scss: {
                additionalData: `@import "@/assets/sass/app.scss"`
            },
        }
    }
}