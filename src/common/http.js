import axios from "axios";

const baseOption = {
    baseURL: 'http://localhost:1337/api/',
    headers: {
        Accept: 'application/json',
    },
};

export const http = axios.create({
    ...baseOption,
});

const axiosPlugin = {
    install(Vue) {
        Vue.prototype.$http = http;
    },
};

let GlobalVue = null;

if (typeof window !== 'undefined') {
    GlobalVue = window.Vue;
} else if (typeof global !== 'undefined') {
    GlobalVue = global.Vue;
}

if (GlobalVue) {
    GlobalVue.use(axiosPlugin);
}


export default axiosPlugin;
