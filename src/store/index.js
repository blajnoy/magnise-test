import Vue from 'vue';
import Vuex from 'vuex';
import {http} from '@/common/http';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        vehicles: {
            isFetching: true,
            items: [],
        },
        vehicle: {
            isFetching: true,
            data: {},
        },
        isUploadFetching: false,
        visibleItemsNum: 7,
        isShownMore: false,
    },
    getters: {
        getVehicles: state => state.isShownMore ? state.vehicles.items : state.vehicles.items.slice(0, state.visibleItemsNum),
        getVehicle: state => state.vehicle.data,
        getIsShownMore: state => state.isShownMore,
        getAllVehiclesLength: state => state.vehicles.items.length,
        getVisibleItemsNum: state => state.visibleItemsNum,
    },
    mutations: {
        setVehicles: (state, data) => state.vehicles.items = data,
        setVehicle: (state, data) => state.vehicle.data = data,
        setUploadFetching: (state, status) => state.isUploadFetching = status,
        toggleShownMore: (state) => state.isShownMore = !state.isShownMore,
    },
    actions: {
        fetchVehicles({commit}, params= {}) {
            return new Promise((resolve, reject) => {
                http.get(`/vehicles`, {...params})
                    .then(res => {
                        commit('setVehicles', res.data);
                        resolve(res);
                    })
                    .catch(err => {
                        console.error('fetchVehicles::', err);
                        reject(err);
                    });
            });
        },
        fetchVehicle({commit}, id) {
            return new Promise((resolve, reject) => {
                http.get(`/vehicles/${id}`)
                    .then(res => {
                        commit('setVehicle', res.data[0]);
                        resolve(res);
                    })
                    .catch(err => {
                        console.error('fetchVehicle::', err);
                        reject(err);
                    });
            });
        },
        postVehicle({commit}, data) {
            return new Promise((resolve, reject) => {
                http.post(`/vehicles`, data)
                    .then((res) => {
                        commit('setUploadFetching', false);
                        resolve(res);
                    })
                    .catch(err => {
                        console.error('postVehicles::', err);
                        reject(err);
                    });
            });
        },
        patchVehicle({commit}, {id, data}) {
            return new Promise((resolve, reject) => {
                http.patch(`/vehicles/${id}`, data)
                    .then((res) => {
                        commit('setUploadFetching', false);
                        resolve(res);
                    })
                    .catch(err => {
                        console.error('patchVehicle::', err);
                        reject(err);
                    });
            });
        },
        deleteVehicle({commit}, id) {
            return new Promise((resolve, reject) => {
                http.delete(`/vehicles/${id}`)
                    .then(res => {
                        commit('setUploadFetching', false);
                        resolve(res);
                    })
                    .catch(err => {
                        console.error('vehicleDelete::', err);
                        reject(err);
                    })
            });
        },
    },
    modules: {},
});
