import '../assets/sass/app.scss';
import Vue from 'vue'
import App from './App.vue'
import store from './store';
import http from '@/common/http';

Vue.config.productionTip = false

Vue.use(http);

new Vue({
  store,
  render: h => h(App),
}).$mount('#app');